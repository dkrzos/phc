<?php
/**
 * Template Name: One column, no sidebar
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>
	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		
		<div class="page-content onecolumn">
	<?php get_template_part( 'loop', 'page' ); ?>
		</div>
		<?php include("partners.php"); ?>		
	</div>
<?php get_footer(); ?>