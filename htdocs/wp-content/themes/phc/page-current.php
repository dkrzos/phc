﻿<?php
/**
 * Template Name: Current development
 * MultiEdit: Hotel, Factilities, Gallery, Directions
 *
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>
	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		<div class="sidebar">
			<?php get_sidebar(); ?>
		</div>
		<div class="hotel page-content">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
					<h1><?php the_title(); ?></h1>
					<!--<h2 class="blue">podtytuł</h2>	-->
					
					<div id="map-current">
						<div class="city szczecin inactive" id="slide-1">
							<span class="city-name"><?php _e('Szczecin','phc'); ?></span>
						</div>
						<div class="city gdansk inactive" id="slide-2">
							<span class="city-name"><?php _e('Gdańsk','phc'); ?></span>
						</div>
						<div class="city bydgoszcz inactive" id="slide-3">
							<span class="city-name"><?php _e('Bydgoszcz','phc'); ?></span>
						</div>
						<div class="city warszawa" id="slide-4">
							<span class="city-name"><?php _e('Warszawa','phc'); ?></span>
						</div>
						<div class="city lublin inactive" id=" slide-5">
							<span class="city-name"><?php _e('Lublin','phc'); ?></span>
						</div>
						<div class="city lodz inactive" id="slide-6">
							<span class="city-name"><?php _e('Łódź','phc'); ?></span>
						</div>
						<div class="city poznan inactive" id=" slide-7">
							<span class="city-name"><?php _e('Poznań','phc'); ?></span>
						</div>
						<div class="city zielona-gora inactive" id=" slide-8">
							<span class="city-name"><?php _e('Zielona Góra','phc'); ?></span>
						</div>
						<div class="city wroclaw inactive" id="slide-9">
							<span class="city-name"><?php _e('Wrocław','phc'); ?></span>
						</div>						
						<div class="city opole inactive" id=" slide-10">
							<span class="city-name"><?php _e('Opole','phc'); ?></span>
						</div>			
						<div class="city katowice inactive" id="slide-11">
							<span class="city-name"><?php _e('Katowice','phc'); ?></span>
						</div>
						<div class="city krakow inactive" id=" slide-12">
							<span class="city-name"><?php _e('Kraków','phc'); ?></span>
						</div>
					</div>
				
	
					<?php the_content(); ?>
							
								
				</article>
				
					
			<?php endwhile; ?>
			
		</div>
		<?php include("partners.php"); ?>
		
	</div>

<?php get_footer(); ?>