﻿<?php
/**
 * Template Name: Hotel page
 * MultiEdit: Hotel, Factilities, Gallery, Directions
 *
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>
	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		<div class="sidebar">
			<?php get_sidebar(); ?>
		</div>
		<div class="hotel page-content">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
					<h1><?php the_title(); ?></h1>
					<!--<h2 class="blue">podtytuł</h2>	-->
					
					<?php if ( has_post_thumbnail() ) {
						echo '<div class="image-thumb">';
						the_post_thumbnail('hotel-thumb');
						echo '</div>'; 
					} ?>
					<?php the_content(); ?>
							
								
				</article>
				
					
			<?php endwhile; ?>
			<div id="tabs">
				<ul class="tabs">
					<?php if(get_post_meta($post->ID, "Hotel", true)) { ?>
						<li><a href="#hotel"><?php _e('Hotel','phc'); ?></a></li>
					<?php } ?>
					<?php if(get_post_meta($post->ID, "Factilities", true)) { ?>
						<li><a href="#factilities"><?php _e('Wyposażenie','phc'); ?></a></li>
					<?php } ?>
					<?php if(get_post_meta($post->ID, "Gallery", true)) { ?>
						<li><a href="#gallery"><?php _e('Galeria','phc'); ?></a></li>
					<?php } ?>
					<?php if(get_post_meta($post->ID, "MapLatitude", true)&& get_post_meta($post->ID, "MapLongitude", true)) { ?>
						<li><a id="directions" href="#directions"><?php _e('Lokalizacja','phc'); ?></a></li>
					<?php } ?>
				</ul>
				<?php //get_template_part( 'loop', 'page' ); ?>
				
				<?php if(get_post_meta($post->ID, "Hotel", true)) { ?>
					<div class="tab hotel">
						<?php echo get_post_meta($post->ID, "Hotel", $single = true); ?>
					</div>
				<?php } ?>
				
				
				<?php if(get_post_meta($post->ID, "Factilities", true)) { ?>
					<div class="tab factilities">
						<?php echo get_post_meta($post->ID, "Factilities", $single = true); ?>
					</div>
				<?php } ?>
				
				
				<?php if(get_post_meta($post->ID, "Gallery", true)) { ?>
					<div class="tab gallery">
						<?php echo apply_filters('the_content', get_post_meta($post->ID, "Gallery", $single = true)); ?>
					</div>
				<?php } ?>
				
				<?php if(get_post_meta($post->ID, "MapLatitude", true)&& get_post_meta($post->ID, "MapLongitude", true)) { ?>
					<div class="tab directions" >
						<div id="mapka" style="width: 610px; height: 400px; border: 1px solid black; background: gray;">
						</div>
						<?php if(get_post_meta($post->ID, "MapLatitude", true)) { ?>
						<span id="latitude" style="display: none;"><?php echo apply_filters('the_content', get_post_meta($post->ID, "MapLatitude", $single = true)); ?></span>
						<?php } ?>
						<?php if(get_post_meta($post->ID, "MapLongitude", true)) { ?>
						<span id="longitude" style="display: none;"><?php echo apply_filters('the_content', get_post_meta($post->ID, "MapLongitude", $single = true)); ?></span>
						<?php } ?>
						<?php if(get_post_meta($post->ID, "MapText", true)) { ?>
						<span id="map-text" style="display: none;"><?php echo apply_filters('the_content', get_post_meta($post->ID, "MapText", $single = true)); ?></span>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php include("partners.php"); ?>
		
	</div>

<?php get_footer(); ?>