<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>
<?php
$l=ICL_LANGUAGE_CODE;
switch ($l){
	case 'en':
		$cat_id = '6';
		break;			
	case 'pt-pt':
		$cat_id = '9';
		break;
	case 'pl':
		$cat_id = '5';
		break;
	default:
		$cat_id = '6';
		break;
	}
?>

	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		<div class="sidebar">
			<h3><a href="<?php echo get_category_link( $cat_id ); ?>"><?php _e('Aktualności','phc'); ?></a></h3>
			<ul class="archive"><?php wp_get_archives('type=monthly&limit=12'); ?></ul>
			
		</div>
		<div class="page-content">
	<?php get_template_part( 'loop', 'single' ); ?>
		</div>
		<?php include("partners.php"); ?>		
	</div>
<?php get_footer(); ?>