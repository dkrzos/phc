﻿$(document).ready(function() {
	$('#page-header .menu>li').hover(function(){
		$(this).find('ul').stop(true, true).toggle(300);
	
	});
	var $slider =$('#slider').cycle({ 
		fx:     'fade', 
		speed:  1000, 
		timeout: 4000,
		startingSlide: 2,  
		before: function(currSlideElement, nextSlideElement, options, forwardFlag){
			txt=$(currSlideElement).attr('class').replace("slide ","");
			$('#map .city.'+txt+' .marker').stop().fadeOut(300);
			txt=$(nextSlideElement).attr('class').replace("slide ","");
			$('#map .city.'+txt+' .marker').stop().fadeIn(300);
		}	
	});
	$('#map .city').hover(function (){
		$this=$(this);
		var nr=parseInt($this.attr('id').replace("slide-",""));
		$slider.cycle(nr-1);
		$slider.cycle('pause'); 
	},function (){
		$this=$(this);
		$slider.cycle('resume'); 
	});
	

	
	$('.tabs li').click(function(){
		var $this=$(this);
		var href=$this.find('a').attr('href').replace("#","");
		//alert(href);
		$('.tab').hide();	
		$('.tab.'+href).show();
		$('.tabs li').removeClass('active');
		$this.addClass('active');
		
		if (href=='directions'){
			lat=$('#latitude').text();
			lon=$('#longitude').text();
			txt=$('#map-text').text();
			mapaStart(lat,lon, txt);
		}
		return false;
	});
	$('.tabs li:first-child').click();
	$('.ngg-gallery-thumbnail a').append('<div class="zoom"></div>');
	$('.ngg-gallery-thumbnail a').hover(function(){
		var $this=$(this);
		$this.find('.zoom').stop().fadeIn();
		$this.find('img').stop().animate({opacity: 0.5});
	},function(){
		var $this=$(this);
		$this.find('.zoom').stop().fadeOut();
		$this.find('img').stop().animate({opacity: 1});
	});
	 // zmienna globalna
	$('#directions').click(function(){
		
	});
});
function mapaStart(lat,lng,txt)   
{   
    var wspolrzedne = new google.maps.LatLng(lat,lng);
    var opcjeMapy = {
      zoom: 13,
      center: wspolrzedne,
      mapTypeId: google.maps.MapTypeId.HYBRID
    };
     
    mapa = new google.maps.Map(document.getElementById("mapka"), opcjeMapy); 
	var marker = dodajMarker(lat,lng,txt);
}   

function dodajMarker(lat,lng,txt)
{
    var opcjeMarkera =   
    {  
        position: new google.maps.LatLng(lat,lng),  
        map: mapa
    }  
    var marker = new google.maps.Marker(opcjeMarkera);
    marker.txt=txt;
    
	dymek = new google.maps.InfoWindow();
	dymek.setContent(marker.txt);
    dymek.open(mapa,marker);

    return marker;
}
