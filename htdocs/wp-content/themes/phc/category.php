<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>
<?php
$l=ICL_LANGUAGE_CODE;
switch ($l){
	case 'en':
		$cat_id = '6';
		break;			
	case 'pt-pt':
		$cat_id = '9';
		break;
	case 'pl':
		$cat_id = '5';
		break;
	default:
		$cat_id = '6';
		break;
	}
?>
	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		<div class="sidebar">
			<h3><a href="<?php echo get_category_link( $cat_id ); ?>"><?php _e('Aktualności','phc'); ?></a></h3>
			<ul class="archive"><?php wp_get_archives('type=monthly&limit=12'); ?></ul>
			<?php// get_sidebar(); ?>
			
		</div>
		<div class="page-content">
				
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '' . $category_description . '';

				get_template_part( 'loop', 'category' );
				?>

		</div>
		<?php include("partners.php"); ?>		
	</div>
<?php get_footer(); ?>