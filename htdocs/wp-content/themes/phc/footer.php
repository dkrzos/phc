<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>
	
	
	<footer>

<?php
	get_sidebar( 'footer' );
?>

		 <?php wp_nav_menu(); ?>
		<span id="copyright"><?php _e('Copyright 2012 Polish Hotel Company. All rights reserved','phc'); ?></span>
		<span id="created"><?php _e('Created by','phc'); ?> <a href="http:// mki.pl">MKI</a></span>
	</footer>
	</div> <!-- #body-container END -->
</div> <!-- #container END -->
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>