﻿<?php
/**
 * Template Name: Mainpage
 *
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php
$l=ICL_LANGUAGE_CODE;
switch ($l){
	case 'en':
		$column1 = '297';
		$column2 = '300';
		$column1more = '86';
		$column2more = '62';
		$lang='en';
		$warszawa='45';
		break;			
	case 'pt-pt':
		$column1 = '294';
		$column2 = '301';
		$column1more = '87';
		$column2more = '63';
		$lang='pt';
		$warszawa='50';
		break;
	case 'pl':
		$column1 = '291';
		$column2 = '296';
		$column1more = '84';
		$column2more = '59';
		$lang='pl';
		$warszawa='26';
		break;
	default:
		$column1 = '297';
		$column2 = '300';
		$column1more = '86';
		$column2more = '62';
		$lang='en';
		$warszawa='45';
		break;
}
		?>


	<div id="mapslider">
		<div id="slider">
			<div class="slide szczecin">			
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/szczecin.jpg" />
				<span class="city-name"><?php _e('Szczecin','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>		
			
			<div class="slide gdansk">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/gdansk.jpg" />
				<span class="city-name"><?php _e('Gdańsk','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 2</li>
					</ul>
				</div>			
			</div>
			<div class="slide bydgoszcz">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/bydgoszcz.jpg" />
				<span class="city-name"><?php _e('Bydgoszcz','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>	
			<div class="slide warszawa">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/warszawa.jpg" />
				<span class="city-name"><?php _e('Warszawa','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W zarządzaniu','phc'); ?>: 1</li>
						<li><?php _e('W budowie','phc'); ?>: 1</li>
						<li><?php _e('W trakcie zamawiania','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide lublin">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/lublin.jpg" />
				<span class="city-name"><?php _e('Lublin','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide lodz">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/lodz.jpg" />
				<span class="city-name"><?php _e('Łódź','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide poznan">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/poznan.jpg" />
				<span class="city-name"><?php _e('Poznań','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W trakcie zamawiania','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide zielona-gora">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/zielona-gora.jpg" />
				<span class="city-name"><?php _e('Zielona Góra','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W trakcie zamawiania','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide wroclaw">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/wroclaw.jpg" />
				<span class="city-name"><?php _e('Wrocław','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 2</li>
					</ul>
				</div>			
			</div>
			<div class="slide opole">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/opole.jpg" />
				<span class="city-name"><?php _e('Opole','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W trakcie zamawiania','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide katowice">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/katowice.jpg" />
				<span class="city-name"><?php _e('Katowice','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			<div class="slide krakow">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider/krakow.jpg" />
				<span class="city-name"><?php _e('Kraków','phc'); ?></span>
				<div class="city-info">
					<span class="header"><?php _e('Holiday Inn Express','phc'); ?></span>
					<ul class="info">
						<li><?php _e('W budowie','phc'); ?>: 1</li>
						<li><?php _e('W trakcie zamawiania','phc'); ?>: 1</li>
					</ul>
				</div>			
			</div>
			
			
		</div>
		<div id="map" class="<?php echo $lang; ?>">
			<div class="city szczecin inactive" id="slide-1">
				<a href="#" class="city-name"><?php _e('Szczecin','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Szczecin','phc'); ?></span></div>
			</div>
			<div class="city gdansk inactive" id="slide-2">
				<a href="#" class="city-name"><?php _e('Gdańsk','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Gdańsk','phc'); ?></span></div>
			</div>
			<div class="city bydgoszcz inactive" id="slide-3">
				<a href="#" class="city-name"><?php _e('Bydgoszcz','phc'); ?></a>
				<div class="marker"><span class="city-name"  style="font-size: 13px;"><?php _e('Bydgoszcz','phc'); ?></span></div>
			</div>
			<div class="city warszawa" id="slide-4">
				<a href="<?php echo get_permalink( $warszawa ); ?>" class="city-name"><?php _e('Warszawa','phc'); ?></a>
				<a href="<?php echo get_permalink( $warszawa ); ?>"><div class="marker"><span class="city-name"  style="font-size: 13px;"><?php _e('Warszawa','phc'); ?></span></div></a>
			</div>
			<div class="city lublin inactive" id=" slide-5">
				<a href="#" class="city-name"><?php _e('Lublin','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Lublin','phc'); ?></span></div>
			</div>
			<div class="city lodz inactive" id="slide-6">
				<a href="#" class="city-name"><?php _e('Łódź','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Łódź','phc'); ?></span></div>
			</div>
			<div class="city poznan inactive" id=" slide-7">
				<a href="#" class="city-name"><?php _e('Poznań','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Poznań','phc'); ?></span></div>
			</div>
			<div class="city zielona-gora inactive" id=" slide-8">
				<a href="#" class="city-name"><?php _e('Zielona Góra','phc'); ?></a>
				<div class="marker"><span class="city-name" style="font-size: 12px;"><?php _e('Zielona Góra','phc'); ?></span></div>
			</div>
			<div class="city wroclaw inactive" id="slide-9">
				<a href="#" class="city-name"><?php _e('Wrocław','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Wrocław','phc'); ?></span></div>
			</div>
			
			<div class="city opole inactive" id=" slide-10">
				<a href="#" class="city-name"><?php _e('Opole','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Opole','phc'); ?></span></div>
			</div>			
			<div class="city katowice inactive" id="slide-11">
				<a href="#" class="city-name"><?php _e('Katowice','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Katowice','phc'); ?></span></div>
			</div>
			<div class="city krakow inactive" id=" slide-12">
				<a href="#" class="city-name"><?php _e('Kraków','phc'); ?></a>
				<div class="marker"><span class="city-name"><?php _e('Kraków','phc'); ?></span></div>
			</div>
			
			
			
			
			
			
		</div>
	
	</div>
	
	<div id="infobox" class="box">
		<div class="column first about">
			<a class="green" href="<?php echo get_permalink( $column1more ); ?>">
				<h2 class="blue"><?php _e('Company','phc'); ?></h2>
				<h2 class="green"><?php _e('Description','phc'); ?></h2>
			</a>
			<?php $my_query = new WP_Query("page_id=".$column1.""); ?>
				<?php if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<div class="lead"><?php the_content(); ?><a class="green" href="<?php echo get_permalink( $column1more ); ?>">&raquo;</a></div>
							
			<?php endwhile; endif; ?>	
		</div>
		<div class="column second">
			<a class="green" href="<?php echo get_permalink( $column2more ); ?>">		
				<h2 class="green"><?php _e('Holiday Inn Express','phc'); ?></h2>
				<h2 class="blue"><?php _e('Roll-Out Poland','phc'); ?></h2>
			</a>
			<?php $my_query = new WP_Query("page_id=".$column2.""); ?>
				<?php if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<div class="lead"><?php the_content(); ?><a class="green" href="<?php echo get_permalink( $column2more ); ?>">&raquo;</a></div>
									
			<?php endwhile; endif; ?>
		</div>
		<div class="column third">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/img/photo-hotel.jpg" />
		</div>
		
	</div>
	
	<?php include("partners.php"); ?>
	
	
	<?php //get_template_part( 'loop', 'page' ); ?>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>