<?php
/**
 * The template for displaying attachments.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
 
get_header(); ?>
	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		<div class="sidebar">
			<?php get_sidebar(); ?>
			
		</div>
		<div class="page-content">
    <?php get_template_part( 'loop', 'attachment' ); ?>
 		</div>
		<?php include("partners.php"); ?>		
	</div>
<?php get_footer(); ?>