<?php
/**
 * Template Name: 404 page
 * MultiEdit: Hotel, Factilities, Gallery, Directions
 *
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>
	<div class="breadcrumbs"> <?php if(function_exists('bcn_display'))  {
        bcn_display();
    }?></div>
	<div id="content">
		<div class="sidebar">
			
			
		</div>
		<div class="page-content">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>				
				<h1><?php _e('Nie znaleziono strony','phc'); ?></h1>
			</header>				
				<p><?php _e('Adres który podałeś odwołuje się do strony, która nie istnieje.','phc'); ?></p>
			<footer>
				<?php edit_post_link( _e( 'Edit', 'starkers' ), '', '' ); ?>
			</footer>
		</article>
		</div>
		<?php include("partners.php"); ?>
	</div>

<?php get_footer(); ?>
