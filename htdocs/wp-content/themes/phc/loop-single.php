<?php
/**
 * The loop that displays a single post.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<header>
				<h1><?php the_title(); ?></h1>

				<?php starkers_posted_on(); ?>
			</header>

			<?php the_content(); ?>
					
			<?php wp_link_pages( array( 'before' => '<nav>' . _e( 'Pages:', 'starkers' ), 'after' => '</nav>' ) ); ?>
		
		
				
		</article>
		
		<nav>
			<span class="older"><?php previous_post_link( '%link',  _x( '&lt; Poprzedni wpis', 'Previous post link', 'starkers' )  ); ?></span>
			<span class="newer"><?php next_post_link( '%link',  _x( 'Następny wpis &gt;', 'Next post link', 'starkers' )); ?></span>
		</nav>

		<?php //comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>